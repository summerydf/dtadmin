function loginFun() {
    if($("#username").val() === ''){
        toastr.warning("抱歉，登录账号不能为空！");
        return false;
    }
    if($("#password").val() === ''){
        toastr.warning("抱歉，登录密码不能为空！");
        return false;
    }
    var formatData = $("#form").serialize();
    $.ajax({
        type: "post",
        url: "/login",
        data: formatData,
        dataType: "json",
        success: function (res) {
            console.log(res);
            if(res.code === 2000){
                toastr.success("恭喜你，登录成功了！");
                window.location.href = "/admin"
            }else {
                toastr.error(res.msg);
            }
        },
        error: function () {
            console.log("服务器异常，请联系管理员！")
        }
    })
}