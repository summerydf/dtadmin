package com.mcro.intellect;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类入口主函数
 * @author DT
 */
@SpringBootApplication
@MapperScan("com.mcro.intellect.mapper")
public class IntellectApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntellectApplication.class, args);
    }

}
