package com.mcro.intellect.mapper;

import com.mcro.intellect.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DT
 * @since 2021-03-10
 */
public interface UserMapper extends BaseMapper<User> {

}
