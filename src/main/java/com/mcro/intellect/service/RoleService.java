package com.mcro.intellect.service;

import com.mcro.intellect.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DT
 * @since 2021-03-10
 */
public interface RoleService extends IService<Role> {

}
