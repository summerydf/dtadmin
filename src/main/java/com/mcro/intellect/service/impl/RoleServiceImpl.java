package com.mcro.intellect.service.impl;

import com.mcro.intellect.entity.Role;
import com.mcro.intellect.mapper.RoleMapper;
import com.mcro.intellect.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT
 * @since 2021-03-10
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
