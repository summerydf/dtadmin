package com.mcro.intellect.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mcro.intellect.entity.Role;
import com.mcro.intellect.entity.User;
import com.mcro.intellect.mapper.UserMapper;
import com.mcro.intellect.service.RoleService;
import com.mcro.intellect.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT
 * @since 2021-03-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService, UserDetailsService {

    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession session;
    @Autowired
    private RoleService roleService;

    /**
     * 用户登录逻辑和验证处理
     * @param username 账号
     * @return 返回登录信息
     * @throws UsernameNotFoundException 登录失败异常
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // 通过用户名查询用户
        User user = userService.getOne(new QueryWrapper<User>().eq("username", username));

        // 放入session
        session.setAttribute("loginUser",user);

        // 创建一个新的UserDetails对象，最后验证登陆的需要
        UserDetails userDetails = null;
        if(user!=null){
            // 登录后会将登录密码进行加密，然后比对数据库中的密码，数据库密码需要加密存储！
            String password = user.getPassword();
            // 创建一个集合来存放权限
            Collection<GrantedAuthority> authorities = getAuthorities(user);
            // 实例化UserDetails对象
            userDetails = new org.springframework.security.core.userdetails.User(username,password,authorities);
        }else {
            throw new UsernameNotFoundException("用户名不存在！");
        }
        return userDetails;
    }

    /**
     * 获取角色信息
     * @param user
     * @return
     */
    private Collection<GrantedAuthority> getAuthorities(User user){
        List<GrantedAuthority> authList = new ArrayList<>();
        Role role = roleService.getById(user.getRoleId());
        authList.add(new SimpleGrantedAuthority(role.getRoleName()));
        return authList;
    }
}
