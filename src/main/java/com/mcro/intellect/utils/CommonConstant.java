package com.mcro.intellect.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 公共操作成功常量码
 * @author: ydf
 * @date: 2021/1/12 1:08
 * @version: v1.0
 */
public class CommonConstant{

    public static final Map<String, Integer> CODE = new HashMap<String, Integer>() {
        private static final long serialVersionUID = 1L;
        {
            put("success", 2000);
            put("error", 5000);
        }
    };

    public static final Map<String, String> MSG = new HashMap<String, String>() {
        private static final long serialVersionUID = 1L;
        {
            put("success_msg", "操作成功！");
            put("error_msg", "操作失败！");
        }
    };


}
