package com.mcro.intellect.utils;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * @description: 自定义常用工具类
 * @author: ydf
 * @date: 2021/1/12 0:52
 * @version: v1.0
 */
public class DtUtils {

    static boolean printFlag = true;

    /**
     * 随机生成UUID
     * @return 字符串
     */
    public static String getUuid(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    /**
     * 获取当前时间
     * @return 时间date
     */
    public static Timestamp getTime(){
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 自定义控制台输出
     * @param msg 内容
     */
    public static void print(String msg){
        if (printFlag){
            System.out.println("dt:=>"+msg);
        }
    }

}
