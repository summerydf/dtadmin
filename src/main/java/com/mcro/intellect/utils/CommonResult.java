package com.mcro.intellect.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: 全局统一返回类型
 * @author: ydf
 * @date: 2021/1/12 0:52
 * @version: v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> implements Serializable {

    private static final long serialVersionUID = 3194875321804368471L;

    /**
     * 响应状态码
     */
    private Integer code;

    /**
     * 描述信息
     */
    private String msg;

    /**
     * 数据
     */
    private T data;

    /**
     * 操作成功
     * @param data 数据
     * @param <T> 泛型
     * @return 返回对象
     */
    public static <T> CommonResult<T> success(T data) {
       return new CommonResult<>(CommonConstant.CODE.get("success"), CommonConstant.MSG.get("success_msg"), data);
    }

    public static <T> CommonResult<T> ok(Integer code, String msg, T data) {
        return new CommonResult<>(code, msg , data);
    }

    public static <T> CommonResult<T> success() {
        return new CommonResult<>(CommonConstant.CODE.get("success"), CommonConstant.MSG.get("success_msg"), null);
    }

    /**
     * 操作失败
     * @param <T> 泛型
     * @return 返回对象
     */
    public static <T> CommonResult<T> error() {
        return new CommonResult<>(CommonConstant.CODE.get("error"), CommonConstant.MSG.get("error_msg"), null);
    }

    public static <T> CommonResult<T> fail(Integer code, String msg) {
        return new CommonResult<>(code, msg , null);
    }

}
