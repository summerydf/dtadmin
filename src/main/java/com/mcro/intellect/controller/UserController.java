package com.mcro.intellect.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mcro.intellect.entity.User;
import com.mcro.intellect.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DT
 * @since 2021-03-10
 */
@Controller
@RequestMapping("/intellect/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 查询用户集合
     * @return 返回集合
     */
    @GetMapping("/list")
    public String userList(Model model){
        Page<User> pageParam = new Page<>(1, 10);
        userService.page(pageParam,new QueryWrapper<User>().orderByDesc("create_time"));
        // 返回结果集
        List<User> userList = pageParam.getRecords();
        model.addAttribute("userList",userList);
        model.addAttribute("pageParam",pageParam);
        return "admin/user/list";
    }

    /**
     * 分页查询用户集合
     * @param page 当前页
     * @param limit 每页记录数
     * @param model 视图
     * @return 返回分页集合
     */
    @GetMapping("/list/{page}/{limit}")
    public String userListPage(@PathVariable int page, @PathVariable int limit, Model model){
        if (page < 1){
            page = 1;
        }
        Page<User> pageParam = new Page<>(page, limit);
        userService.page(pageParam,new QueryWrapper<User>().orderByDesc("create_time"));
        // 返回结果集
        List<User> userList = pageParam.getRecords();
        model.addAttribute("userList",userList);
        model.addAttribute("pageParam",pageParam);
        return "admin/user/list";
    }

}

