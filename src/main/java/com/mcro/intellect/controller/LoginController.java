package com.mcro.intellect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mcro.intellect.entity.User;
import com.mcro.intellect.service.UserService;
import com.mcro.intellect.utils.DtUtils;
import com.mcro.intellect.vo.RegisterFormVO;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @description: 登录控制器
 * @author: ydf
 * @date: 2021/3/1 21:49
 * @version: v1.0
 */
@Controller
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping({"/","/index"})
    public String index(){
        return "views/index";
    }

    @GetMapping("/admin")
    public String admin(){
        return "admin/index";
    }

    @GetMapping("/index1")
    public String index1(){
        return "admin/index1";
    }

    @GetMapping("/index2")
    public String index2(){
        return "admin/index2";
    }

    @GetMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    @GetMapping("/register")
    public String toRegister(){
        return "register";
    }

    /**
     * 注册用户信息
     * @param registerForm 注册信息实体类
     * @param model 视图
     * @return 返回视图
     */
    @PostMapping("/register")
    public String register(RegisterFormVO registerForm, Model model){
        DtUtils.print("注册表单信息："+registerForm.toString());
        // 表单密码重复判断
        if (!registerForm.getPassword().equals(registerForm.getRepassword())){
            model.addAttribute("registerMsg","密码输入有误");
            return "register";
        }
        // 用户名已存在
        User hasUser = userService.getOne(new QueryWrapper<User>().eq("username", registerForm.getUsername()));
        if (hasUser!=null){
            model.addAttribute("registerMsg","用户名已存在");
            return "register";
        }

        // 构建用户对象
        User user = new User();
        // 普通用户权限
        user.setRoleId(2);
        user.setUsername(registerForm.getUsername());
        // 密码加密
        String bCryptPassword = new BCryptPasswordEncoder().encode(registerForm.getPassword());
        user.setPassword(bCryptPassword);
        user.setCreateTime(DtUtils.getTime());
        user.setUpdateTime(DtUtils.getTime());

        // 保存对象
        userService.save(user);
        DtUtils.print("新用户注册成功："+user);

        // 注册成功，重定向到登录页面
        return "redirect:/toLogin";
    }


}
