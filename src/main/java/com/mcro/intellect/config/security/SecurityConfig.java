package com.mcro.intellect.config.security;

import com.mcro.intellect.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * <p>
 * security配置
 * </p>
 *
 * @author DT
 * @since 2021-02-19
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyLogoutSuccessHandler logoutSuccessHandler;
    @Autowired
    private MyAuthenticationProvider authenticationProvider;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private LoginSuccessHandler loginSuccessHandler;
    @Autowired
    private LoginFailureHandler loginFailureHandler;

    /**
     * 密码加密方式
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 自定义认证配置
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    /**
     * 请求授权验证
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 访问权限
        http.authorizeRequests()
                .antMatchers("/","/index","/test/**").permitAll()
                .antMatchers("/register","/login","/toLogin").permitAll()
                .antMatchers("/*").authenticated();

        // 登录配置
        http.formLogin()
                .usernameParameter("username")
                .passwordParameter("password")
                // 设置登录页面
                .loginPage("/toLogin")
                // 登录表单提交请求
                .loginProcessingUrl("/login");


        // 设置userDetailsService，处理用户信息(注意：当我们配置了自定义认证的逻辑处理后，请删除以下配置，防止出现两次重复查询认证操作)
        // http.userDetailsService(userService);

        // 登录异常处理（适用于前后端分离下使用，或者form表单由ajax提交，返回具体的json响应信息）
        http.formLogin()
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler);


        // 单用户登录，如果有一个登录了，同一个用户在其他地方登录将前一个剔除下线
        // http.sessionManagement().maximumSessions(1).expiredSessionStrategy(MyExpiredSessionStrategy());
        // 配置session管理,失效后跳转，单用户登录，如果有一个登录了，同一个用户在其他地方不能登录
        // http.sessionManagement().invalidSessionUrl("/toLogin").maximumSessions(1).maxSessionsPreventsLogin(true);

        // session失效配置
        http.sessionManagement().invalidSessionUrl("/toLogin");

        // 注销退出配置
        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(logoutSuccessHandler)
                .deleteCookies("JESSIONID");

        // 跨域
        http.headers().frameOptions().disable();
        // 关闭csrf
        http.csrf().disable();

        // 记住我配置
        http.rememberMe()
                .tokenValiditySeconds(18000)
                .tokenRepository(persistentTokenRepository())
                .userDetailsService(userService);
    }

    /**
     * 配置TokenRepository
     * @return
     */
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        // 配置数据源
        jdbcTokenRepository.setDataSource(dataSource);
        // 第一次启动的时候自动建表（可以不用这句话，自己手动建表，源码中有语句的）
        // jdbcTokenRepository.setCreateTableOnStartup(true);
        return jdbcTokenRepository;
    }

}