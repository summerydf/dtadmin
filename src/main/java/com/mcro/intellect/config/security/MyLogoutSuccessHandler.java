package com.mcro.intellect.config.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description: 自定义退出逻辑处理器
 * @author: ydf
 * @date: 2021/3/10 11:30
 * @version: v1.0
 */
@Component("logoutSuccessHandler")
@Slf4j
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        log.info("自定义退出逻辑处理器：{}",authentication);
        // 重定向到登录页
        httpServletResponse.sendRedirect("/toLogin");
    }
}
