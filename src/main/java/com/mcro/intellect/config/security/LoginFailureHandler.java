package com.mcro.intellect.config.security;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mcro.intellect.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @description: 自定义登录认证失败处理器
 * @author: ydf
 * @date: 2021/3/10 10:38
 * @version: v1.0
 */
@Component("LoginFailureHandler")
@Slf4j
public class LoginFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        log.info("登录失败处理器：{}",e.getMessage());
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        ServletOutputStream out = httpServletResponse.getOutputStream();
        String message;
        int code = 5001;
        if(e instanceof UsernameNotFoundException){
            message = "账号不存在，登录失败!";
        }else if(e instanceof BadCredentialsException){
            message = "密码错误，登录失败!";
        }else{
            message = "登录失败!";
        }
        String res = JSONObject.toJSONString(CommonResult.fail(code,message), SerializerFeature.DisableCircularReferenceDetect);
        out.write(res.getBytes(StandardCharsets.UTF_8));
        out.flush();
        out.close();
    }
}
