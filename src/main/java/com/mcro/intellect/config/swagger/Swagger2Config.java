package com.mcro.intellect.config.swagger;

import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 * localhost:8080/swagger-ui.html
 * </p>
 *
 * @author DT
 * @since 2021-02-19
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

	/**
	 * 是否启用
	 */
	@Value("${swagger.enable}")
	private Boolean enable;

	@Bean
	public Docket webApiConfig(){
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("DT-Api")
				.apiInfo(webApiInfo())
				.enable(enable)
				.select()
				.paths(Predicates.not(PathSelectors.regex("/error.*")))
				.build();
	}

	private ApiInfo webApiInfo(){
		return new ApiInfoBuilder()
				.title("Api")
				.description("Api接口")
				.version("1.0")
				.contact(new Contact("api", "http://api.com", "api@qq.com"))
				.build();
	}

}