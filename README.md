# dtadmin

#### 介绍
DT ADMIN 开源管理系统，V1.0.0 正式开源

#### 软件架构

# 一、核心架构

|架构|版本  |
|--|--|
| 技术核心 |SpringBoot 2.3.5  |
| 安全框架|Spring Security 2.3.5  |
| 数据库|MySQL 8.0.17  |
| JDK|11  |
| 持久层|MybatisPlus 3.4.0  |
| 缓存|redis 5.0  |
| 前端UI|bootstrap 4.0  |

注意：这是单体版的管理系统，后续在技术上会加入各种开发常用的技术框：比如消息队列，ES搜索引擎等等，目前只是初始版本。


#### 使用说明

1.  导入mcrodt.sql脚本
2.  修改config/security/SecurityConfig配置文件（第一次启动程序，打开该段配置注释）
    // 第一次启动的时候自动建表（可以不用这句话，自己手动建表，源码中有语句的）
    // jdbcTokenRepository.setCreateTableOnStartup(true);
3.  账号密码：admin/admin

4.  代码生成器为test包下面的CodeGeneratorTest.class 修改数据库，即可执行

5.  swagger接口文档：默认地址>>>localhost:8080/swagger-ui.html

